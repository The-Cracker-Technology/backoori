rm -rf /opt/ANDRAX/bin/backoori
rm -rf /opt/ANDRAX/backoori

go build -o backoori main.go

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build backoori... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip backoori

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip... PASS!"
else
  # houston we have a problem
  exit 1
fi

mkdir -p /opt/ANDRAX/backoori/crafter

cp -Rf backoori /opt/ANDRAX/backoori/backoori

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf crafter/agent_plate.ps1 /opt/ANDRAX/backoori/crafter

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy agent crafter... PASS!"
else
  # houston we have a problem
  exit 1
fi

mkdir -p /opt/ANDRAX/backoori/output

cp -Rf resources /opt/ANDRAX/backoori/resources

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy resources... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
